var inputs = renderInputs(
	filtracja,
	new Input({
		label: "r_0",
		description: "Promień studni",
		unit: "m"
	}),
	new Input({
		label: "k",
		description: "Współczynnik filtracji",
		unit: "m/s"
	}),
	new Input({
		label: "H",
		description: "Głębokość studni",
		unit: "m"
	}),
	new Input({
		label: "Q",
		description: "Dopływ wody do studni",
		unit: "m3/s"
	})
);


function filtracja() {
	return exp.mul(
		inputs["r_0"],
		exp.pow(
			exp.terminal(Math.E).label("e"),
			exp.frac(
				exp.mul(
					exp.terminal(Math.PI).label("\\Pi"),
					inputs["k"],
					exp.pow(inputs["H"], 2)
				),
				inputs["Q"]
			)
		)
	).label("R");
}
