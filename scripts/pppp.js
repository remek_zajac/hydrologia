var inputs = renderInputs(
	pppp,
	new Input({
		label: "L",
		description: "Dlugosc cieku",
		unit: "km"
	}),
	new Input({
		label: "l",
		description: "Dlugosc suchej doliny do dzialu wodnego",
		unit: "km"
	}),
	new Input({
		label: "msk",
		description: "Miara szorstkosci koryta",
		unit: "km",
  		//https://www.nfosigw.gov.pl/download/gfx/nfosigw/pl/nfoekspertyzy/858/2/1/2009-56.pdf
  		//Tabela B.2. Miara szorstkości koryta cieku m
		list: [{
				value: 11,
				key: "Koryta stałych i okresowych rzek nizinnych o stosunkowo wyrównanym dnie"
			},{
				value: 9,
				key: "Koryta stałych i okresowych rzek wyżynnych meandrujacych o częściowo nierównym dnie"
			},{
				value: 7,
				key: "Koryta stałych i okresowych rzek górskich o bardzo nierównymotoczakowo - kamienistym dnie"
			}]
	}),
	new Input({
		label: "W_g",
		description: "Wzniesinie dzialu wodnego, rzedna gorna",
		unit: "m"
	}),
	new Input({
		label: "W_d",
		description: "Wzniesinie przekroju zamykajacego, rzedna dolna",
		unit: "m"
	}),
	new Input({
		label: "A",
		description: "Powierzchnia zlewni",
		unit: "km2"
	}),
	new Input({
		label: "\\phi",
		description: "Wspolczynnik odplywu, zalezny od rodzaju gleby"
	}),
	new Input({
		label: "H_1",
		description: "Maksymalny opad dobowy o prawdopodobienstwie przewyzszenia 1% odczytany z mapy",
		unit: "mm"
	}),
	new Input({
		label: "M_s",
		description: "Miara szorstkosci stokow odczytana z tabeli"
	}),
	new Input({
		label: "\\Delta_h",
		description: "Roznica wysokosci miedzy warstwicami o ustalonej wysokosci",
		unit: "m"
	}),
	new Input({
		label: "\\sum_k",
		description: "Suma dlugosci wszystkich warstwic w zlewni",
		unit: "km"
	})
);


function pppp() {
	variables["Jrl"] = exp.mul(
		0.6,
		exp.div(
			exp.subt(inputs["W_g"], inputs["W_d"]),
			exp.add(inputs["L"],inputs["l"])
		)
	).label("J_{rl}")
	 .describe("Uśredniony spadek cieku")
	 .inline(true);

	variables["\\Phi_r"] = exp.div(
		exp.mul(1000.0,
				exp.sum(inputs["L"],inputs["l"])
		),
		exp.mul(
			inputs["msk"],
			exp.pow(variables["Jrl"].flatten(),exp.frac(1,3)),
			exp.pow(inputs["A"],exp.frac(1,4)),
			exp.pow(
				exp.mul(
					inputs["\\phi"],
					inputs["H_1"]
				),exp.frac(1,4)
			)
		)
	).label("\\Phi_r")
 	 .describe("Hydromorfologiczna charakterystyka koryta cieku");

	variables["Ls"] = exp.div(
		inputs["A"],
		exp.mul(
			1.8,exp.add(inputs["L"],inputs["l"])
		)
	).label("Ls")
 	 .describe("Średnia długość stoków")
 	 .inline(true);

	variables["Js"] = exp.div(
		exp.mul(inputs["\\Delta_h"],inputs["\\sum_k"]),
		inputs["A"]
	).label("Js")
	 .describe("Średni spadek stoków")
	 .inline(true);

	variables["\\Phi_s"] = exp.div(
		exp.pow(
			exp.mul(1000.0,variables["Ls"].flatten()),
			exp.frac(1,2)
		),
		exp.mul(
			inputs["M_s"],
			exp.pow(
				variables["Js"].flatten(),
				exp.frac(1,4)
			),
			exp.pow(
				exp.mul(
					inputs["\\phi"],
					inputs["H_1"]
				),
				exp.frac(1,2)
			)
		)
	).label("\\Phi_s")
	 .describe("Hydromorfologiczna charakterystyka stoków");

	variables["ts"] = new exp.Terminal(
		fis2ts(variables["\\Phi_s"].value())
	).label("ts")
	 .describe("Czas spływu po stokach")
	 .inline(true);

	variables["f1"]  = new exp.Terminal(
		f1(variables["\\Phi_r"].value(), variables["ts"].value())
	).label("f1")
	 .describe("Maksymalny moduł odpływu jednostkowego określony na podstawie hydromorfologicznej charakterystyki koryta cieku")
	 .inline(true);

	/**
	 * Przeplyw o praW_dopododobienstwie przewyzszenia p %
	 *
	 * Qp%  - przepływ wielkiej wody o określonym praW_dopodobieństwie przewyższenia
	 * f=0,6-bezwymiarowy współczynnik kształtu fali wezbraniowej
	 * F1-maksymalny moduł odpływu jednostkowego określony na podstawie hydromorfologicznej charakterystyki koryta cieku
	 * φ=0,50-wspólczynnik odpływu zależny od rodzaju gleb, przyjęty z tab.5.18 „Metody obliczeń przepływów maksymalnych w małych zlewniach  rzecznych” Andrzej Ciepielewski
	 * H1=90mm maksymalny opad dobowy o praW_dopodobieństwie pojawienia się p=1%
	 * (Atlas Hydrologiczny Polski 1987r.)
	 * A=0,20km2 – Math.powierzchnia zlewni
	 * λp= kwantyl rozkładu zmiennej λp odczytany z tablic dla regionu 4b i praW_dopodobieństwa p% tab.5.19 „Metody obliczeń...”
	 * δ1-współczynnik redukcji jeziornej
	 */
	return exp.mul(
		0.6,
		variables["f1"],
		inputs["\\phi"],
		inputs["H_1"],
		inputs["A"],
		1.0,
		1.0
	).label("Q_p");
 }


function fis2ts(fis) {
	var fisVals = [0.5,1.0,1.5,2.0,2.5,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,12.0,15.0];
	var tsVals  = [2.4,5.2,8.2,11.0,16.0,20.0,31.0,43.0,58.0,74.0,93.0,113,140,190,287];

	var factor  = value2factor(fisVals, fis);
	return interpolate(
	    tsVals[factor.smallerIdx],
	    tsVals[factor.biggerIdx],
	    factor.factor
	);
}
