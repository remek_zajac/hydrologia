/**
 * https://www.nfosigw.gov.pl/download/gfx/nfosigw/pl/nfoekspertyzy/858/2/1/2009-56.pdf
 *
 * Tabela B.6. Wartości funkcji F1 w zależności od czasu spływu po stokach ts i hydromorfologicznej charakterystyki koryta r
 */

function value2factor(series, value) {
  for (var biggerIdx = 0; biggerIdx < series.length && value > series[biggerIdx]; biggerIdx++) {}
  var smallerIdx = biggerIdx;
  if (value != series[biggerIdx] && biggerIdx > 0) {
    smallerIdx = biggerIdx - 1;
  }
  var delta  = series[biggerIdx] - series[smallerIdx];
  return {
    factor: delta ? (value - series[smallerIdx])/delta : 1,
    smallerIdx: smallerIdx,
    biggerIdx: biggerIdx
  };
}

function interpolate(left, right, factor) {
  return left + (right-left)*factor;
}

function f1(fiR, ts) {
  var axes={
    fiR:[5,10,20,30,40,50,60,70,80,90,100,120,150,180,200,250,300,350],
    ts: [10,30,60,100,150,200]
  }

  //Obszar kraju z wyłączeniem Tatr i wysokich gór (H < 700 m n.p.m.)
  var F=[
    [0.305,0.200,0.128,0.0930,0.0720,0.0565,0.0460,0.0385,0.0345,0.0305,0.0265,0.0212,0.0165,0.0134,0.0119,0.00975,0.00830,0.00725],
    [0.170,0.140,0.104,0.0815,0.0645,0.0510,0.0428,0.0360,0.0322,0.0282,0.0249,0.0203,0.0162,0.0132,0.0116,0.00965,0.00825,0.00720],
    [0.120,0.104,0.0830,0.0665,0.0540,0.0444,0.0380,0.0330,0.0300,0.0267,0.0238,0.0195,0.0155,0.0127,0.0114,0.00955,0.00820,0.00710],
    [0.090,0.081,0.0665,0.0545,0.0465,0.0386,0.0336,0.0300,0.0274,0.0246,0.0220,0.0185,0.0152,0.0123,0.0112,0.00940,0.00810,0.00705],
    [0.067,0.062,0.0526,0.0445,0.0380,0.0336,0.0300,0.0270,0.0247,0.0224,0.0204,0.0174,0.0142,0.0118,0.0109,0.00920,0.00790,0.00690],
    [0.053,0.050,0.0433,0.0380,0.0337,0.0300,0.0272,0.0250,0.0228,0.0209,0.0192,0.0165,0.0136,0.0115,0.0107,0.00900,0.00770,0.00680]
  ];

  //Tatry i wysokie góry (H > 700 m n.p.m.)
  var F_Tatry=[
    [0.0120,0.0880,0.0610,0.0468,0.0386,0.0332,0.0290,0.0257,0.0235,0.0216,0.0198,0.0172,0.0146,0.0128,0.0118,0.00975,0.00830,0.00725],
    [0.0844,0.0695,0.0530,0.0427,0.0362,0.0315,0.0278,0.0247,0.0226,0.0209,0.0193,0.0170,0.0144,0.0126,0.0116,0.00965,0.00825,0.00720],
    [0.0624,0.0565,0.0457,0.0380,0.0327,0.0288,0.0260,0.0236,0.0217,0.0200,0.0186,0.0165,0.0141,0.0124,0.0114,0.00955,0.00820,0.00710],
    [0.0492,0.0450,0.0388,0.0338,0.0295,0.0265,0.0240,0.0221,0.0205,0.0190,0.0179,0.0159,0.0138,0.0121,0.0112,0.00940,0.00810,0.00705],
    [0.0404,0.0374,0.0298,0.0298,0.0265,0.0243,0.0223,0.0207,0.0193,0.0181,0.0171,0.0153,0.0134,0.0118,0.0109,0.00920,0.00790,0.00690],
    [0.0342,0.0325,0.0264,0.0264,0.0245,0.0226,0.0211,0.0196,0.0185,0.0175,0.0166,0.0148,0.0129,0.0116,0.0107,0.00900,0.00770,0.00980]
  ];

  var firFactor = value2factor(axes.fiR, fiR);
  var tsFactor  = value2factor(axes.ts,  ts);
  var upper     = interpolate(
    F[tsFactor.smallerIdx][firFactor.smallerIdx],
    F[tsFactor.smallerIdx][firFactor.biggerIdx],
    firFactor.factor
  );
  var lower     = interpolate(
    F[tsFactor.biggerIdx][firFactor.smallerIdx],
    F[tsFactor.biggerIdx][firFactor.biggerIdx],
    firFactor.factor
  );
  return interpolate(upper, lower, tsFactor.factor);
}

