/**
 * scripts/lib/popularity.js
 *
 * This, based on the value of the master slider,
 * calculates and returns the turn preference
 */

'use strict';

function assert(condition, message) {
    if (!condition) {
        throw new Error("Assertion failed; "+message);
    }
}

var exp = (function() {

    function Expression(terms) {
        if (terms) {
            assert(Array.isArray(terms));
            this.mTerms = terms.map(function(term) {
                if (!(term instanceof Expression)) {
                    assert(typeof term === "number");
                    return new Terminal(term);
                }
                return term;
            });
        }
    }

    Expression.prototype.value = function() {
        assert(false, "Not implemented");
    }

    Expression.prototype.explainRhs = function(options) {
        assert(false, "Not implemented");
    }

    Expression.prototype.isEquation = function() {
        return !!this.label() || this.mTerms.reduce(function(cum,nxt) {
            return cum || nxt.isEquation();
        }, false);
    }

    Expression.prototype.explain = function() {
        var result = "";
        if (this.mLabel) {
            result += this.mLabel;
        }

        var expression = this;
        if (this.isEquation()) {
            result += " = " + expression.explainRhs({forceLabels:true});
        }
        while (expression) {
            result += " = " + expression.explainRhs();
            expression = expression.reduce();
        }
        return result;
    }

    Expression.prototype.label = function(label) {
        if (arguments.length > 0) {
            this.mLabel = label;
            return this;
        }
        return this.mLabel;
    }

    Expression.prototype.inline = function(flag) {
        if (arguments.length > 0) {
            this.mInline = flag;
            return this;
        }
        return this.mInline;
    }

    Expression.prototype.describe = function(description) {
        if (arguments.length > 0) {
            this.mDescription = description;
            return this;
        }
        return this.mDescription;
    }

    Expression.prototype.flatten = function() {
        return new Terminal(this.value())
            .label(this.label())
            .describe(this.describe())
    }

    Expression.prototype.reduce = function(cb, collect) {
        var allEvaluated = !!this.mTerms.length;
        var anyReduced   = false;
        this.mTerms = this.mTerms.map(function(term) {
            assert(term instanceof Expression);
            if (term.mTerminal) {
                var value = term.value();
                assert(typeof value === "number");
                if (!isNaN(value)) {
                    if (cb) {
                        collect = cb(value, collect);
                    }
                } else {
                    allEvaluated = false;
                }
            } else {
                allEvaluated = false;
                var reduced = term.reduce();
                if (reduced) {
                    term = reduced;
                    anyReduced = true;
                }
            }
            return term;
        });
        if (allEvaluated) {
            return new Terminal(collect)
        }
        if (anyReduced) {
            return this;
        }
    }





    function Terminal(number) {
        Expression.call(this, []);
        this.mTerminal = true;
        this.mNumber = number;
    }
    Terminal.prototype = new Expression();
    Terminal.prototype.constructor = Terminal;

    Terminal.prototype.value = function(number) {
        if (!number) {
            return this.mNumber;
        }
        assert(typeof number === "number");
        this.mNumber = number;
    }

    Terminal.prototype.explainRhs = function(options) {
        if (
            this.label() &&
            (isNaN(this.value()) || (options && options.forceLabels))
           ) {
            return this.label();
        }
        return Number(
            this.mNumber.toPrecision(6)
        ).toString();
    }

    Terminal.prototype.isEquation = function() {
        return false;
    }






    function Div() {
        assert(arguments.length == 0 || arguments.length == 2);
        Expression.call(this, Array.prototype.concat.apply([], arguments));
    }
    Div.prototype = new Expression();
    Div.prototype.constructor = Div;

    Div.prototype.top = function() {
        return this.mTerms[0];
    }

    Div.prototype.bottom = function() {
        return this.mTerms[1];
    }

    Div.prototype.value = function() {
        return this.top().value()/this.bottom().value();
    }

    Div.prototype.explainRhs = function(options) {
        return "\\frac{"+this.top().explainRhs(options)+"}{"+this.bottom().explainRhs(options)+"}";
    }

    Div.prototype.reduce = function() {
        return Expression.prototype.reduce.call(this, undefined, this.value());
    }



    function Frac(top, bottom) {
        Div.apply(this, [top,bottom]);
        this.mTerminal = true;
    }
    Frac.prototype = new Div();
    Frac.prototype.constructor = Frac;




    function Add() {
        Expression.call(
            this, Array.prototype.concat.apply([], arguments)
        );
    }
    Add.prototype = new Expression();
    Add.prototype.constructor = Add;

    Add.prototype.value = function() {
        return this.mTerms.reduce(function(cum, nxt) {
            return cum + nxt.value();
        }, 0);
    }

    Add.prototype.explainRhs = function(options) {
        return this.mTerms.map(function(term) {
            return term.explainRhs(options);
        }).join(" + ");
    }

    Add.prototype.reduce = function() {
        return Expression.prototype.reduce.call(this, function(value, collect) {
            return collect + value;
        }, 0);
    }





    function Subt(lhs,rhs) {
        Expression.call(this, [lhs,rhs]);
    }
    Subt.prototype = new Expression();
    Subt.prototype.constructor = Subt;

    Subt.prototype.value = function() {
        return this.mTerms[0].value()-this.mTerms[1].value()
    }

    Subt.prototype.explainRhs = function(options) {
        return this.mTerms.map(function(term) {
            return term.explainRhs(options);
        }).join(" - ");
    }

    Subt.prototype.reduce = function() {
        return Expression.prototype.reduce.call(
            this,
            undefined,
            this.value()
        );
    }






    function Mul() {
        Expression.call(this, Array.prototype.concat.apply([], arguments));
    }
    Mul.prototype = new Expression();
    Mul.prototype.constructor = Mul;

    Mul.prototype.value = function() {
        return this.mTerms.reduce(function(cum, nxt) {
            return cum * nxt.value();
        },1.0);
    }

    Mul.prototype.explainRhs = function(options) {
        return this.mTerms.map(function(term) {
            var t = term.explainRhs(options);
            if (term instanceof Add) {
                t = "("+t+")";
            }
            return t;
        }).join(" * ");
    }

    Mul.prototype.reduce = function() {
        return Expression.prototype.reduce.call(this, function(value, collect) {
            return collect * value;
        }, 1);
    }



    function Pow(mantissa, exponent) {
        Expression.call(this, [mantissa, exponent]);
    }
    Pow.prototype = new Expression();
    Pow.prototype.constructor = Pow;

    Pow.prototype.mantissa = function() {
        return this.mTerms[0]
    }

    Pow.prototype.exponent = function() {
        return this.mTerms[1]
    }

    Pow.prototype.value = function() {
        return Math.pow(
            this.mantissa().value(),
            this.exponent().value()
        );
    }

    Pow.prototype.explainRhs = function(options) {
        var mantissa = this.mantissa().explainRhs(options);
        if (!this.mantissa().mTerminal) {
            mantissa = "("+mantissa+")";
        }
        return mantissa + "^" + this.exponent().explainRhs(options);
    }

    Pow.prototype.reduce = function() {
        return Expression.prototype.reduce.call(
            this,
            undefined,
            this.value()
        );
    }

    return {
        terminal: function() {
            var args = Array.prototype.concat.apply([null], arguments);
            return new (Function.prototype.bind.apply(Terminal, args));
        },
        Terminal : Terminal,
        add: function() {
            var args = Array.prototype.concat.apply([null], arguments);
            return new (Function.prototype.bind.apply(Add, args));
        },
        sum: function() {
            var args = Array.prototype.concat.apply([null], arguments);
            return new (Function.prototype.bind.apply(Add, args));
        },
        subt: function() {
            var args = Array.prototype.concat.apply([null], arguments);
            return new (Function.prototype.bind.apply(Subt, args));
        },
        div: function() {
            var args = Array.prototype.concat.apply([null], arguments);
            return new (Function.prototype.bind.apply(Div, args));
        },
        frac: function() {
            var args = Array.prototype.concat.apply([null], arguments);
            return new (Function.prototype.bind.apply(Frac, args));
        },
        mul: function() {
            var args = Array.prototype.concat.apply([null], arguments);
            return new (Function.prototype.bind.apply(Mul, args));
        },
        pow: function() {
            var args = Array.prototype.concat.apply([null], arguments);
            return new (Function.prototype.bind.apply(Pow, args));
        },
    }

})();
