var variables = {};


function Input(options) {
    exp.Terminal.call(this, NaN);
    this.label(options.label).describe(options.description);
    this.mUnit = options.unit || "";
    this.mInputAttrs = "type=\"number\"";
    if (options.list) {
    	var listId  = "datalist_"+options.label;
    	this.mInputAttrs = "type=\"list\" list=\""+listId+"\"";
    	var listDiv = "<datalist id=\""+listId+"\">"+
			options.list.map(function(elem) {
				return "<option value=\""+elem.value+"\">"+elem.key+"</option>"
			}).join("") + "</datalist>";
    	$("body").append(listDiv);
    }
}
Input.prototype = new exp.Terminal();
Input.prototype.constructor = Input;

Input.prototype.render = function(tableDiv) {
	var self = this;
	var inputId  = "input_"+this.mLabel.replace("\\", "");
	var symbolId = "sym_"+this.mLabel.replace("\\", "");
    tableDiv.append(
      "<tr>"+
        "<td id="+symbolId+"><b>$"+this.mLabel+"$</b> ("+this.mDescription+")"+"</td>"+
        "<td><input id=\""+inputId+"\" "+this.mInputAttrs+">"+this.mUnit+"</td>"+
      "</tr>"
    );
    this.mInputDiv = $("#"+inputId);
    this.change(function(value) {
    	self.value(
    		parseFloat(value)
    	)
    });
    setTimeout(function() {
	    $("#"+symbolId+" .MathJax_CHTML").focus(function() {
	    	self.mInputDiv.focus();
	    });
    }, 1000);

}

Input.prototype.change = function(cb) {
	var self = this;
	this.mInputDiv.change(function() {
		cb && cb(self.mInputDiv.val())
	});
}


/*
 * renderInputs takes
 * the calculateImpl function and a list of inputs it expects
 */
function renderInputs(calculateImpl) {
	var result = {};
	var inputsTable  = $("#input");
	for (var i = 1; i < arguments.length; i++) {
		var input = arguments[i];
		result[input.label()] = input;
		input.render(inputsTable);
		input.change(calculate.bind(undefined, calculateImpl));
	}
	return result;
}

function calculate(calculateImpl) {
    var result = calculateImpl(inputs);

 	var diagnostics = "";
 	for (var k in variables) {
 		var variable  = variables[k];
 		var explained = "$"+variable.explain()+"$";
 		if (variable.describe()) {
 			diagnostics += variable.describe() + ": ";
 		}
 		if (!variable.inline()) {
 			explained = "$"+explained+"$";
 		}
 		diagnostics += "<span>"+explained+"</span><br>";
 	}
 	if (diagnostics.length > 0) {
 		diagnostics += "Ostatecznie:";
 	}
 	diagnostics += "<h3><b>$$"+result.explain()+"$$</b></h3><br>";
 	$("#diagnostics").html(diagnostics);
	MathJax.Hub.Typeset();
	$(".mjx-chtml").css("text-align","left");
}
